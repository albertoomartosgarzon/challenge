<?php
// First, run 'composer require pusher/pusher-php-server'
require __DIR__ . '/vendor/autoload.php';

$pusher = new Pusher\Pusher(
    "53b6c4ec5940e4cf31a1", // Replace with 'key' from dashboard
    "3bc3b1e0372db39f59db", // Replace with 'secret' from dashboard
    "1358810", // Replace with 'app_id' from dashboard
    array(
        'cluster' => 'eu' // Replace with 'cluster' from dashboard
    )
);
// Trigger a new random event every second. In your application,
// you should trigger the event based on real-world changes!
while (true) {
    $pusher->trigger('price-btcusd', 'new-price', array(
        'value' => rand(0, 5000)
    ));
    sleep(1);
}
?>